﻿using WebStore.Data;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace WebStore.Services
{
    public class WebStoreContext: DbContext
    {
        private readonly string connectionString;

        public DbSet<Product> Products { get; set; }

        public DbSet<Store> Stores { get; set; }

        public WebStoreContext(string connection)
        {
            connectionString = connection;
            this.Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseLazyLoadingProxies()
                .UseSqlServer(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>()
                .HasOne<Store>(s => s.Store)
                .WithMany(g => g.Products)
                .HasForeignKey(s => s.StoreId)
                .OnDelete(DeleteBehavior.Cascade);

            #region seed data

            var stores = new List<Store>{
                new Store
                {
                    StoreId = 1,
                    Name = "Books store",
                    Address = "Minsk, st. TestStreet 12",
                    OperatingMode = "08:00 - 16:00"
                },
                new Store
                {
                    StoreId = 2,
                    Name = "Test Store",
                    Address = "Minsk, st. TestStreet 13",
                    OperatingMode = "08:00 - 16:00"
                },
                new Store
                {
                    StoreId = 3,
                    Name = "Soft drink shop",
                    Address = "Minsk, st. TestStreet 115",
                    OperatingMode = "08:00 - 16:00"
                }
            };

            var products = new List<Product> {
                new Product
                {
                    ProductId = 1,
                    Name = "Fahrenheit 451, Ray Bradbury",
                    Description = "Book",
                    StoreId = 1
                },
                new Product
                {
                    ProductId = 2,
                    Name = "Atlas Shrugged, Ayn Rand",
                    Description = "Book",
                    StoreId = 1
                },
                new Product
                {
                    ProductId = 3,
                    Name = "test product 1",
                    Description = "Test",
                    StoreId = 2
                },
                new Product
                {
                    ProductId = 4,
                    Name = "test product 2",
                    Description = "Test",
                    StoreId = 2
                },
                new Product
                {
                    ProductId = 5,
                    Name = "Sprite drink, 1 l.",
                    Description = "Sprite is a colorless, caffeine-free, lemon and lime-flavored soft drink created by The Coca-Cola Company.",
                    StoreId = 3
                },
                new Product
                {
                    ProductId = 6,
                    Name = "schweppes drink, 2 l.",
                    Description = "Schweppes is a Swiss beverage brand that is sold around the world. It includes a variety of lemonade, carbonated waters and ginger ales.",
                    StoreId = 3
                }
            };


            modelBuilder.Entity<Store>()
                .HasData(stores);

            modelBuilder.Entity<Product>()
                .HasData(products);

            #endregion
        }
    }
}
