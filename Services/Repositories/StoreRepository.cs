﻿using WebStore.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WebStore.Services
{
    public class StoreRepository : IRepository<Store>
    {
        private readonly WebStoreContext _context;

        public StoreRepository(WebStoreContext context)
        {
            _context = context;
        }

        public Store Create(Store item)
        {
            Store temp = null;

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    temp = _context.Stores.Add(item).Entity;

                    this.Save();
                    transaction.Commit();
                }
                catch
                {
                    temp = null;
                    transaction.Rollback();
                }
            }

            return temp;
        }

        public void Delete(int id)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    Store store = _context.Stores.Find(id);
                    _context.Stores.Remove(store);

                    this.Save();
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
            }
        }

        public Store Get(int id)
        {
            return _context.Stores.Find(id);
        }

        public IEnumerable<Store> GetAll()
        {
            return _context.Stores.ToList();
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Update(Store item)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var temp = _context.Stores.Find(item.StoreId);
                    if (temp != null)
                    {
                        temp.Address = item.Address;
                        temp.Name = item.Name;
                        temp.OperatingMode = item.OperatingMode;
                    }

                    this.Save();
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
            }
        }
    }
}
