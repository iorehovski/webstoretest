﻿using System.Collections.Generic;

namespace WebStore.Services
{
    public interface IRepository<T>
    {
        T Create(T item);

        T Get(int id);

        IEnumerable<T> GetAll();

        void Update(T item);

        void Delete(int id);

        void Save();
    }
}
