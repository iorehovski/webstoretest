﻿using WebStore.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WebStore.Services
{
    public class ProductRepository : IRepository<Product>
    {
        private readonly WebStoreContext _context;

        public ProductRepository(WebStoreContext context)
        {
            _context = context;
        }

        public Product Create(Product item)
        {
            Product temp = null;

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    temp = _context.Products.Add(item).Entity;

                    this.Save();
                    transaction.Commit();
                }
                catch
                {
                    temp = null;
                    transaction.Rollback();
                }
            }

            return temp;
        }

        public void Delete(int id)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    Product product = _context.Products.Find(id);
                    _context.Products.Remove(product);

                    this.Save();
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
            }
        }

        public Product Get(int id)
        {
            return _context.Products.Find(id);
        }

        public IEnumerable<Product> GetAll()
        {
            return _context.Products.ToList();
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Update(Product item)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var temp = _context.Products.Find(item.ProductId);
                    if (temp != null)
                    {
                        temp.Name = item.Name;
                        temp.Description = item.Description;
                    }

                    this.Save();
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
            }
        }
    }
}
