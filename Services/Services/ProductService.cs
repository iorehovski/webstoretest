﻿using WebStore.Data;
using System.Collections.Generic;

namespace WebStore.Services
{
    public class ProductService
    {
        private readonly ProductRepository _repository;

        public ProductService(ProductRepository repository)
        {
            _repository = repository;
        }

        public Product Create(Product item)
        {
            var temp = _repository.Create(item);
            return temp;
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }

        public Product Get(int id)
        {
            return _repository.Get(id);
        }

        public IEnumerable<Product> GetAll()
        {
            return _repository.GetAll();
        }

        public void Update(Product item)
        {
            _repository.Update(item);
        }
    }
}
