﻿using WebStore.Data;
using System.Collections.Generic;

namespace WebStore.Services
{
    public class StoreService
    {
        private readonly StoreRepository _repository;

        public StoreService(StoreRepository repository)
        {
            _repository = repository;
        }

        public Store Create(Store item)
        {
            var temp = _repository.Create(item);
            return temp;
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }

        public Store Get(int id)
        {
            return _repository.Get(id);
        }

        public IEnumerable<Store> GetAll()
        {
            return _repository.GetAll();
        }

        public void Update(Store item)
        {
            _repository.Update(item);
        }
    }
}
