﻿using Autofac;

namespace WebStore.Services
{
    public class ServiceModule: Module
    {
        public string ConnectionString { get; set; }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<WebStoreContext>().AsSelf()
                .WithParameter(new NamedParameter("connection", ConnectionString));

            builder.RegisterType<ProductRepository>();
            builder.RegisterType<StoreRepository>();
            builder.RegisterType<StoreService>();
            builder.RegisterType<ProductService>();
        }
    }
}
