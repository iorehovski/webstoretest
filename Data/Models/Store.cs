﻿using System.Collections.Generic;

namespace WebStore.Data
{
    public class Store
    {
        public int StoreId { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string OperatingMode { get; set; }

        public virtual IList<Product> Products { get; set; }
    }
}
