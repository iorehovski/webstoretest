﻿using System.ComponentModel.DataAnnotations;

namespace WebStore.Data
{
    public class Product
    {
        public int ProductId { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required]
        public int StoreId { get; set; }
        public virtual Store Store { get; set; }
    }
}
