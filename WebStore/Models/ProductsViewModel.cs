﻿using System.Collections.Generic;
using WebStore.Data;

namespace WebStore.Models
{
    public class ProductsViewModel
    {
        public IEnumerable<Data.Product> Products { get; set; }
        public IEnumerable<Data.Store> Stores { get; set; }

        public string StoreName { get; set; }
        public int StoreId { get; set; }

        public bool isEditing { get; set; } = false;

        public Product Product { get; set; }
    }
}
