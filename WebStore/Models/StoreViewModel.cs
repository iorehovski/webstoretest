﻿using WebStore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebStore
{
    public class StoreViewModel
    {
        public IEnumerable<Store> Stores { get; set; }
    }
}
