﻿using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebStore.Services;
using WebStore.Models;
using WebStore.Data;

namespace WebStore.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly StoreService _storeService;
        private readonly ProductService _productService;

        public HomeController(
            ILogger<HomeController> logger,
            StoreService storeService,
            ProductService productService)
        {
            _logger = logger;
            _storeService = storeService;
            _productService = productService;
        }

        public IActionResult Index()
        {
            var model = new StoreViewModel
            {
                Stores = _storeService.GetAll()
            };

            return View(model);
        }

        // Return products of selected store
        public IActionResult Products(int id /* StoreId */)
        {
            var model = new ProductsViewModel
            {
                Products = _productService.GetAll().Where(p => p.StoreId == id),
                Stores = _storeService.GetAll(),
                StoreName = _storeService.Get(id).Name,
                StoreId = id
            };

            return View("Products", model);
        }

        // Remove selected product
        public IActionResult DeleteProduct(int id/* ProductId */)
        {
            _productService.Delete(id);

            return Redirect(Request.Headers["Referer"].ToString());
        }

        [HttpPost]
        public IActionResult CreateProduct(Product temp)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest("Undefined name/storeId");
            }

            _productService.Create(new Product 
            {
                Name = temp.Name,
                Description = temp.Description,
                StoreId = temp.StoreId
            });

            return Redirect(Request.Headers["Referer"].ToString());
        }

        // Edit product. Fill edit form.
        public IActionResult EditProduct(int id /* ProductId */)
        {
            var product = _productService.Get(id);

            var model = new ProductsViewModel
            {
                Products = _productService.GetAll().Where(p => p.StoreId == product.StoreId),
                Stores = _storeService.GetAll(),
                StoreName = product.Store.Name,
                StoreId = product.StoreId,
                isEditing = true,
                Product = product
            };

            return View("Products", model);
        }

        [HttpPost]
        public IActionResult UpdateProduct(Product temp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Undefined name/storeId");
            }

            _productService.Update(temp);
            
            return Products(temp.StoreId);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
